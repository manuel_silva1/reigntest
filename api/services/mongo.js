let mongoose = require('mongoose');

//const server = '127.0.0.1:27017'; // REPLACE WITH YOUR DB SERVER
const server = 'reigntest_mongo_1:27017' //DOCKER
const database = 'reignTest';      // REPLACE WITH YOUR DB NAME

class Database {
  constructor() {
    mongoose.set('useCreateIndex', true)
    this._connect()
  }
  
_connect() {
     mongoose.connect(`mongodb://${server}/${database}`, {useNewUrlParser: true, useUnifiedTopology: true})
       .then(() => {
         console.log('Database connection successful')
       })
       .catch(err => {
         console.error('Database connection error')
       })

       var db = mongoose.connection;
       db.on('error', console.error.bind(console, 'connection error:'));
       db.once('open', function() {
         // we're connected!
         console.log("We are connected")
       });
  }
}

module.exports = new Database()