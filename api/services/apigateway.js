//Load the request module
var request = require("request");
var hackerNewsSchema = require("./../models/hackerNew");
require("./../utils/constants");

function initializeRecurrent() {
  requestRecurrent()
  setInterval(function() {
    try {
      requestRecurrent()
    } catch (error) {}
  }, 3600000);
}

function requestRecurrent() {
  request(
    {
      url: hackerNewsUrl, //URL to hit

      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    },
    function(error, response, body) {
      if (error) {
        console.log(error);
      } else {
        if (response.statusCode === 200) {
          var newsJson = JSON.parse(body);
          newsJson["hits"].forEach(element => {
            hackerNewsSchema.find({ objectID: element.objectID }).then(doc => {
              if (doc.length !== 0) {
              } else {
                hackerNewsSchema.create(element, function(error, doc) {
                  if (error) console.log(error);
                });
              }
            });
          });
        }
      }
    }
  );
}

module.exports = initializeRecurrent;
