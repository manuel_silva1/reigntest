require("./services/mongo");
var initializeRecurrent = require("./services/apigateway");
const express = require("express");
var router = require("./routes/router.js");

var newModel = require("./models/hackerNew");

// Constants
const PORT = 8080;
const HOST = "0.0.0.0";

// App
const app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  //Comment if cors package is unavaible
  /*  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    ("Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header();
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
 */
  next();
});

app.use("/v1", router);
initializeRecurrent()
/* newModel.remove({}, (err)=>{console.log(err)}) */
app.listen(PORT, HOST);
