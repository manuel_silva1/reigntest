let mongoose = require("mongoose");
//let validator = require("validator");

let hackerNewSchema = new mongoose.Schema({
  created_at: Date,
  title: String,
  url: String,
  author: String,
  points: Number,
  story_text: String,
  comment_text: String,
  num_comments: Number,
  story_id: Number,
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: Number,
  _tags: Array,
  objectID: {
    type: String,
    unique: true,
    required: true
  },
  _highlightResult: Object,
  isValid: Boolean
});

//Pre
hackerNewSchema.pre("save", function(next) {
  this.isValid = true;
  next();
});

//GET ALL
hackerNewSchema.statics.getNews = function() {
  return new Promise((resolve, reject) => {
    this.find()
      .sort({ created_at: "desc" })
      .exec((err, docs) => {
        if (err) {
          console.error(err);
          return reject(err);
        }

        resolve(docs);
      });
  });
};

//UPDATE NEW
hackerNewSchema.statics.invalidNew = function(newId) {
  console.log("BUSCANDO", newId);
  return new Promise((resolve, reject) => {
    this.updateOne({ _id: newId }, { isValid: false }, (err, docs) => {
      if (err) {
        console.error(err);
        return reject(err);
      }
      resolve(docs);
    });
  });
};

hackerNewSchema.statics.validNew = function(newId) {
  console.log("BUSCANDO VALIDAR", newId);
  return new Promise((resolve, reject) => {
    this.updateOne({ _id: newId }, { isValid: true }, (err, docs) => {
      if (err) {
        console.error(err);
        return reject(err);
      }
      resolve(docs);
    });
  });
};

module.exports = mongoose.model("HackerNew", hackerNewSchema);
