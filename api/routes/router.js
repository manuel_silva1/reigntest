const express = require("express");
var router = express.Router();
let hackerNewModel = require(".././models/hackerNew");
var cors = require("cors");

router.use(cors())

router.get("/hackernews", (req, res) => {
  hackerNewModel
    .getNews()
    .then(docs => {
      res.json({ data: docs });
    })
    .catch(err => {
      res.status(500).send({
        errorMessage: err
      });
    });
});

router.put("/hackernew", cors(), (req, res) => {
  console.log("Printing", req.body);
  let newFound;

  if (!("isVisible" in req.body)) {
    return res.status(400).send({
      success: "false",
      message: "isVisible is Required"
    });
  }

  if (!req.body.id) {
    return res.status(400).send({
      success: "false",
      message: "id is Required"
    });
  }
  if (req.body.isVisible)
    hackerNewModel.validNew(req.body.id).then(docs => {
      console.log(docs);
      newFound = docs;
      if (!newFound) {
        return res.status(404).send({
          success: "false",
          message: "todo not found"
        });
      } else {
        return res.status(201).send({
          success: "true",
          message: "todo added successfully"
        });
      }
    });
  else
    hackerNewModel.invalidNew(req.body.id).then(docs => {
      console.log(docs);
      newFound = docs;
      if (!newFound) {
        return res.status(404).send({
          success: "false",
          message: "New not found"
        });
      } else {
        return res.status(201).send({
          success: "true",
          message: "New added successfully"
        });
      }
    });
});

module.exports = router;
