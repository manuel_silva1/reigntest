import React, { Fragment } from "react";
import "./App.css";

import { TopHeader } from "./screens/topHeader";
import { ListViewNews } from "./screens/listView";

function App() {
  return (
    <Fragment>
      <TopHeader />
      <ListViewNews />
    </Fragment>
  );
}

export default App;
