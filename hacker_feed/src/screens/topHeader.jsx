import React from "react";

import {
  Container,
  Typography,
  Grid,
  createMuiTheme,
  MuiThemeProvider
} from "@material-ui/core";

const colortheme = createMuiTheme({
  palette: {
    primary: { main: "#fff", contrastText: "#3f2c7f" },
    secondary: { main: "#03a9f4", contrastText: "#fff" }
  }
});

export function TopHeader() {
  return (
    <Container disableGutters={true} maxWidth="xl">
      <Typography
        component="div"
        style={{ backgroundColor: "#3f2c7f", height: "15vh" }}
      >
        <Grid container
          direction="column"
          alignItems="center"
          justify="center"
          className="grid-title"
        >
          <Grid item xs={11} align="center">
            <Typography
              component="div"
              style={{ backgroundColor: "#3f2c7f", height: "3vh" }}
            />
          </Grid>
          <Grid item xs={11} align="center">
            <MuiThemeProvider theme={colortheme}>
              <Typography variant="h2" component="h3" color="primary">
                HackerNews Feed
              </Typography>
            </MuiThemeProvider>
            <Typography
              component="div"
              style={{ backgroundColor: "#3f2c7f" }}
            />
          </Grid>
        </Grid>
      </Typography>
    </Container>
  );
}
