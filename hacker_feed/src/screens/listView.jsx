import React, {
  useState,
  useEffect,
  Fragment,
  useContext,
  useReducer
} from "react";
import axios from "axios";
import {
  Container,
  Typography,
  List,
  ListItem,
  CircularProgress,
  Card,
  CardContent,
  Grid,
  IconButton
} from "@material-ui/core";
import { Delete } from "@material-ui/icons";

import hackerNewUrl from "./../services/apigateway";
import moment from "moment";

import { TransitionGroup, CSSTransition } from "react-transition-group";

import "./listView.css";

const delay = ms => new Promise(res => setTimeout(res, ms));

export function ListViewNews() {
  const [data, setData] = useState({ hits: [] });
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      const result = await axios(hackerNewUrl + "/hackernews");
      setData(Array.from(result.data["data"]).filter(elem => elem.isValid));
      await delay(500);
      setIsLoading(false);
    };

    fetchData();
  }, []);

  return (
    <Container disableGutters={true} maxWidth="xl">
      <Fragment>
        {isLoading ? (
          <div className="loading-indicator">
            <CircularProgress style={progressIndicatorStyle} />
          </div>
        ) : (
          <ListComponent news={data} />
        )}
        <Typography
          component="div"
          style={{ backgroundColor: "#FAFBFC", height: "70vh" }}
        />
      </Fragment>
    </Container>
  );
}

const NewsDispatch = React.createContext(null);

function reducer(state, action) {
  switch (action.type) {
    case "remove":
      return { deleted: [...state.deleted, action.value] };
    default:
      throw new Error();
  }
}

function ListComponent(props) {
  const [state, dispatch] = useReducer(reducer, { deleted: [] });

  const items = Array.from(props.news)
    .filter(
      elem =>
        (elem.title !== null || elem.story_title !== null) &&
        !state.deleted.includes(elem._id)
    )
    .map((elem, index) => {
      return (
        <CSSTransition
          key={elem._id}
          in={true}
          appear={true}
          classNames="example"
          timeout={{ appear: 500, enter: 500, exit: 300 }}
        >
          <Fragment key={elem._id}>
            <ListCard data={elem} index={index} />
          </Fragment>
        </CSSTransition>
      );
    });

  return (
    <NewsDispatch.Provider value={dispatch}>
      <List dense className={"listNews"}>
        <TransitionGroup>{items}</TransitionGroup>
      </List>
    </NewsDispatch.Provider>
  );
}

function ListCard(props) {
  const [isloading, setIsLoading] = useState(false);
  const [isHover, setisHover] = useState(false);

  const dispatch = useContext(NewsDispatch);

  function formatDate(date) {
    var newDate = moment.utc().format(date);
    if (moment.utc().diff(moment(newDate), "days") > 7)
      return moment(newDate).format("MMM Do YYYY");
    else return moment(newDate).fromNow();
  }

  async function handleDeleteClick() {
    setIsLoading(true);
    var urlPost = hackerNewUrl + `hackernew`;
    let config = {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      }
    };

    let data = {
      isVisible: false,
      id: props.data._id
    };

    axios
      .put(urlPost, data, config)
      .then((res, err) => {
        if (res) {
          if (res.status === 201) {
            setIsLoading(false);
            dispatch({ type: "remove", value: props.data._id });
            return;
          }
        } else {
          if (err) {
          }
        }
        setIsLoading(false);
      })

      .catch(e => {
        console.log("catch", e);
        setIsLoading(false);
      });
  }

  function routeTo(url) {
    window.open(url); //This will open a new windows with the url
  }

  function onMouseEnter() {
    setisHover(true);
  }

  function onMouseLeave() {
    setisHover(false);
  }
  return (
    <ListItem key={props.data._id}>
      <Card
        raised={isHover}
        style={cardStyle}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={11}>
              <div
                onClick={() =>
                  props.data.story_url !== null
                    ? routeTo(props.data.story_url)
                    : props.data.url !== null
                    ? routeTo(props.data.url)
                    : routeTo("http://www.google.com")
                }
              >
                <Grid container spacing={2}>
                  <Grid item xs={9}>
                    <Typography variant="h6" component="h2">
                      {props.data.story_title !== null
                        ? props.data.story_title
                        : props.data.title}
                    </Typography>

                    <Typography color="textSecondary">
                      {props.data.author}
                    </Typography>
                  </Grid>
                  <Grid item xs={3}>
                    {formatDate(props.data.created_at)}
                  </Grid>
                </Grid>
              </div>
            </Grid>
            <Grid item xs>
              {isloading ? (
                <CircularProgress
                  style={progressIndicatorStyleDelete}
                  color="secondary"
                />
              ) : (
                <IconButton
                  children={<Delete color="secondary" />}
                  onClick={handleDeleteClick}
                />
              )}
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </ListItem>
  );
}

var cardStyle = {
  display: "block",
  width: "90vw",
  height: '13vh',
  margin: "auto",
  transitionDuration: "0.3s"
};

var progressIndicatorStyle = {
  width: "100px",
  height: "100px"
};

var progressIndicatorStyleDelete = {
  width: "40px",
  height: "40px"
};
