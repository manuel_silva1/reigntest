# ReignTest

Test for ReignDesign - By Manuel Silva Cavieres

# Requirements

*  docker
*  docker-compose

# Summary

Docker setup includes an nginx for react client, where you can access thought localhost.
Includes an image for mongoDB and an image for the api in expressJS where they connect throught containers

# Instructions

If you meet the requirements, just execute in command terminal `sudo docker-compose up --build` and let the containers build. Afterwards, go to your favorite browser and type localhost

# Considerations

1.  For the ExpressJS api I used a setInterval function that runs every 1 hour. In a real case scenario, the correct way to approach this background task is using nodeJS threading (workers) to execute this task using Redis as DB. For the sake of the example I used this method
2.  For the ExpressJS api I used a framework to work with mongoDB, called mongoose.
3.  The way I approach the "deletion" of news is using a flag inside documents. Whenever a document is inserted in the pre creation I set up a variable isVisible to true. When a user deletes the news, it is no longer visible, ir order to avoid re-inserting to DB.
4.  For the client, I used React with only hooks for state management and CSSTransitions for animating the news List.